package Pertemuan_tiga;

public class Lcd {
    private String status,cable;
    private int volume,brightness;
    private String [] opsicable = {"VGA","DVI","HDMI","DISPLAY PORT"};
    public Lcd(){
        this.status="menyala";
        this.volume=30;
        this.brightness=60;
        this.cable="VGA";
        
    }
    public void turnOff(){
        this.status="mati";
    }
    public void turnOn(){
        this.status="menyala";
    }
    public void Freeze(){
        this.status="Freeze";
    }
    public void volumeUp(){
        this.volume++;
    }
    public void volumeDown(){
        this.volume--;
    }
    public void setVolume(int volume){
        this.volume=volume;
    }
    public void brightnessUp(){
        this.brightness++;
    }
    public void brightnessDown(){
        this.brightness--;
    }
    public void setBrightness(int Brightness){
        this.brightness=Brightness;
    }
    public void cableUp(){
        for (int i = 0; i < opsicable.length; i++) {
            if(cable.equals(opsicable[i])){
                if(i==opsicable.length-1){
                    this.cable=opsicable[0];
                }else{
                    this.cable=opsicable[i+1];
                }
            }
        }
    }
    public void cableDown(){
        for (int i = 0; i < opsicable.length; i++) {
            if(cable.equals(opsicable[i])){
                if(i==0){
                    this.cable=opsicable[3];
                }else{
                    this.cable=opsicable[i-1];
                }
            }
        }
    }
    public void setCable(String cable){
        this.cable=cable;
    }
    public void DisplayLcd(){
        System.out.println("========== LCD ==========");
        System.out.println("Status     : "+status);
        System.out.println("Volume     : "+volume);
        System.out.println("Brightness : "+brightness);
        System.out.println("Cable      : "+cable);
    }
}
